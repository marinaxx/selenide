import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest {
    @Test
    public void doThms() {
        open("https://www.bbc.com/news");
        $("#idcta-username").click();
        $("#user-identifier-input").click();
        $("#user-identifier-input").setValue("Marina");
        $("#password-input").setValue("password").pressEnter();
    }
    @Test
        public void surfBlogs(){
        open("http://trashisfortossers.com/about-lauren/");
        $(byText("Bea Johnson")).click();
        $("#search-btn > div > a > span").click();
        $("#menu-item-5828 > a").hover();
        }
    @Test
    public void check() {
        open("http://trashisfortossers.com/about-lauren/");
        $("#page > div > footer > div:nth-child(2) > div > div > div > div > form > div > span > button > span:nth-child(2) > i").scrollTo();
        $("#page > div > footer > div:nth-child(2) > div > div > div > div > form > div > span > button").click();
        $("#mergeRow-0").shouldHave(text("Email Address "));
        $("#mergeRow-1 > label").shouldHave(text("First Name"));
    }
    @Test
    public void rome(){
        open("https://selectitaly.ru/search/museums/%D0%A0%D0%B8%D0%BC/7?");
        $("#catId").innerText();
        $(By.xpath("//*[@id=\"lead-navigation\"]/ul/li[3]/a")).shouldBe(exist).innerText().equals("ЭКСКУРСИИ");
    }
    @Test
    public void buy(){
        open("https://www.google.com/");
        $(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input")).setValue("internet-bilet.ua").pressEnter();
        $("#rso > div:nth-child(1) > div > div > div > div > div.r > a > h3 > span").click();
        $("#topNav > ul > li:nth-child(1) > a").click();
        $("#searchInput").shouldBe(empty);
        $("#searchInput").setValue("Пожалел дурак дурочку").shouldBe(visible);
        $("#searchInput").getText();
        $("#content_inner > h1").equals("РЕЗУЛЬТАТЫ ПОИСКА ПО ЗАПРОСУ ПОЖАЛЕЛ ДУРАК ДУРОЧКУ");
    }


}
